const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  console.log(req)
  res.json({
    id: 2000,
    name: 'Iphone'
  })
})

app.get('/hellome', function (req, res) {
  res.send('Hello me')
})

app.listen(port, () => {
  console.log('Example app listening on port 3000')
})
